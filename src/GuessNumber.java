import java.util.Random;
import java.util.Scanner;

public class GuessNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean playAgain = true;

        while (playAgain) {
            Random random = new Random();
            int randomNumber = random.nextInt(100) + 1;
            int guess = 0;
            int numGuesses = 0;

            System.out.println("我已经想好了一个1到100之间的整数，请猜一下是多少：");

            while (numGuesses < 7) {
                guess = scanner.nextInt();
                numGuesses++;

                if (guess == randomNumber) {
                    System.out.println("恭喜你猜对了！你一共猜了" + numGuesses + "次。");
                    break;
                } else if (guess < randomNumber) {
                    System.out.println("你猜的数字太小了，请再猜一次：");
                } else {
                    System.out.println("你猜的数字太大了，请再猜一次：");
                }
            }

            if (numGuesses == 7 && guess != randomNumber) {
                System.out.println("很遗憾，你没有在规定次数内猜对。正确答案是" + randomNumber + "。");
            }

            System.out.println("是否再玩一局？输入Y继续，输入其他任意字符结束游戏。");
            String playAgainInput = scanner.next();
            playAgain = playAgainInput.equalsIgnoreCase("Y");
        }

        System.out.println("游戏结束。");
    }
}
